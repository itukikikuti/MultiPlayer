﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        Player health = collision.gameObject.GetComponent<Player>();
        if (health != null)
        {
            health.TakeDamage(collision.relativeVelocity.sqrMagnitude);
        }

        Destroy(this);
    }
}

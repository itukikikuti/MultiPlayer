﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering.PostProcessing;

public class Player : NetworkBehaviour
{
    [SerializeField] float jumpForce = 5f;
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] Camera camera;
    [SerializeField] Transform weapon;
    [SerializeField] Transform muzzle;
    [SerializeField] GameObject bullet;

    Vector3 cameraPosition;
    Vector3 cameraAngles;
    float timer;
    float speed;
    float sensitivity;
    [SyncVar] float health = 10000f;

    void Start()
    {
        if (!isLocalPlayer)
            return;

        camera.enabled = true;
        camera.GetComponent<AudioListener>().enabled = true;
        camera.GetComponent<PostProcessLayer>().enabled = true;
        camera.GetComponent<PostProcessVolume>().enabled = true;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void OnDestroy()
    {
        if (!isLocalPlayer)
            return;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        if (!isLocalPlayer)
            return;

        timer += Time.deltaTime;
        if (timer > 0.1f && Input.GetButton("Fire1"))
        {
            timer = 0f;
            CmdFire();
        }

        if (Input.GetButton("Fire2"))
        {
            weapon.localPosition = Vector3.Lerp(weapon.localPosition, new Vector3(0f, -0.124f, 0.39f), 0.3f);
            camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, 60f, 0.3f);
            sensitivity = Mathf.Lerp(sensitivity, 2f, 0.3f);
        }
        else
        {
            weapon.localPosition = Vector3.Lerp(weapon.localPosition, new Vector3(0.12f, -0.23f, 0.39f), 0.3f);
            camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, 90f, 0.3f);
            sensitivity = Mathf.Lerp(sensitivity, 5f, 0.3f);
        }

        cameraAngles += new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * sensitivity;
        cameraAngles.x = Mathf.Clamp(cameraAngles.x, -90f, 90f);

        camera.transform.localEulerAngles = new Vector3(cameraAngles.x, 0f, 0f);
        this.transform.eulerAngles = new Vector3(0f, cameraAngles.y, 0f);

        if (Input.GetButton("Fire3"))
        {
            speed = Mathf.Lerp(speed, 8f, 0.3f);
        }
        else
        {
            speed = Mathf.Lerp(speed, 5f, 0.3f);
        }

        Vector3 right = camera.transform.right;
        Vector3 velocity = (this.transform.forward * Input.GetAxis("Vertical") + right * Input.GetAxis("Horizontal")) * speed;
        rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);

        if (!Physics.CheckSphere(transform.position - new Vector3(0f, 0.7f, 0f), 0.4f))
            return;

        if (Input.GetButtonDown("Jump"))
        {
            velocity = rigidbody.velocity;
            velocity.y = jumpForce;
            rigidbody.velocity = velocity;
        }
    }

    [Command]
    void CmdFire()
    {
        GameObject go = Instantiate(bullet, muzzle.position, Quaternion.identity);
        go.GetComponent<Rigidbody>().velocity = muzzle.forward * 30f;
        Destroy(go, 2f);

        NetworkServer.Spawn(go);
    }

    public void TakeDamage(float amount)
    {
        if (!isServer)
            return;

        health -= amount;

        if (health <= 0f)
        {
            health = 10000f;
            RpcRespawn();
        }
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            transform.position = Vector3.zero;
        }
    }
}
